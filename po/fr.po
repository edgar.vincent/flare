# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the flare package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: flare\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-09 13:04+0200\n"
"PO-Revision-Date: 2023-11-23 16:03+0000\n"
"Last-Translator: rene-coty <irenee.thirion@e.email>\n"
"Language-Team: French <https://hosted.weblate.org/projects/"
"schmiddi-on-mobile/flare/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.2.1-rc\n"

#: data/resources/ui/about.blp.in:13
msgid "translator-credits"
msgstr "Irénée Thirion"

#: data/resources/ui/channel_info_dialog.blp:5 data/resources/ui/window.blp:152
msgid "Channel Information"
msgstr "Informations sur la conversation"

#: data/resources/ui/channel_info_dialog.blp:28
msgctxt "accessibility"
msgid "Reset Session"
msgstr "Réinitialiser la session"

#: data/resources/ui/channel_info_dialog.blp:31
msgid "Reset Session"
msgstr "Réinitialiser la session"

#: data/resources/ui/channel_info_dialog.blp:40
#: data/resources/ui/error_dialog.blp:44
#: data/resources/ui/submit_captcha_dialog.blp:23
msgid "Close"
msgstr "Fermer"

#: data/resources/ui/channel_list.blp:11
#: data/resources/ui/channel_messages.blp:15
msgid "Offline"
msgstr "Hors-ligne"

#: data/resources/ui/channel_list.blp:19
msgid "Search"
msgstr "Rechercher"

#: data/resources/ui/channel_list.blp:22 data/resources/ui/window.blp:60
msgctxt "accessibility"
msgid "Search"
msgstr "Recherche"

#: data/resources/ui/channel_messages.blp:21
msgid "No Channel Selected"
msgstr "Aucune conversation sélectionnée"

#: data/resources/ui/channel_messages.blp:22
msgid "Select a channel"
msgstr "Sélectionnez une conversation"

#: data/resources/ui/channel_messages.blp:91
msgctxt "accessibility"
msgid "Remove the reply"
msgstr "Retirer la réponse"

#: data/resources/ui/channel_messages.blp:94
msgctxt "tooltip"
msgid "Remove reply"
msgstr "Retirer la réponse"

#: data/resources/ui/channel_messages.blp:119
msgctxt "accessibility"
msgid "Remove an attachment"
msgstr "Retirer une pièce jointe"

#: data/resources/ui/channel_messages.blp:122
msgctxt "tooltip"
msgid "Remove attachment"
msgstr "Retirer la pièce jointe"

#: data/resources/ui/channel_messages.blp:153
msgctxt "accessibility"
msgid "Add an attachment"
msgstr "Ajouter une pièce jointe"

#: data/resources/ui/channel_messages.blp:160
msgctxt "tooltip"
msgid "Add attachment"
msgstr "Ajouter une pièce jointe"

#: data/resources/ui/channel_messages.blp:176
msgctxt "accessibility"
msgid "Message input"
msgstr "Saisie du message"

#: data/resources/ui/channel_messages.blp:182
msgctxt "tooltip"
msgid "Message input"
msgstr "Saisie du message"

#: data/resources/ui/channel_messages.blp:192
msgctxt "accessibility"
msgid "Send"
msgstr "Envoyer"

#: data/resources/ui/channel_messages.blp:195
msgctxt "tooltip"
msgid "Send"
msgstr "Envoyer"

#: data/resources/ui/dialog_clear_messages.blp:5
#: data/resources/ui/window.blp:123
msgid "Clear messages"
msgstr "Effacer les messages"

#: data/resources/ui/dialog_clear_messages.blp:6
msgid ""
"This will clear all locally stored messages. This will also close the "
"application."
msgstr ""
"Ceci effacera tous les messages stockés localement et fermera l’application."

#: data/resources/ui/dialog_clear_messages.blp:11
#: data/resources/ui/dialog_unlink.blp:11 src/gui/window.rs:288
msgid "Cancel"
msgstr "Annuler"

#: data/resources/ui/dialog_clear_messages.blp:12
msgid "Clear"
msgstr "Effacer"

#: data/resources/ui/dialog_unlink.blp:5 data/resources/ui/window.blp:118
msgid "Unlink"
msgstr "Délier"

#: data/resources/ui/dialog_unlink.blp:6
msgid ""
"This will unlink this device and remove all locally saved data. This will "
"also close the application such that it can be relinked when opening it "
"again."
msgstr ""
"Cet appareil sera délié et toutes les données enregistrées localement seront "
"supprimées. Cela fermera aussi l’application pour qu’elle puisse être à "
"nouveau reliée à l’ouverture."

#: data/resources/ui/dialog_unlink.blp:12
msgid "Unlink and delete all messages"
msgstr "Délier et supprimer tous les messages"

#: data/resources/ui/dialog_unlink.blp:13
msgid "Unlink but keep messages"
msgstr "Délier mais conserver les messages"

#: data/resources/ui/error_dialog.blp:5
msgid "Error"
msgstr "Erreur"

#: data/resources/ui/error_dialog.blp:26
msgid "Please consider reporting this error."
msgstr "Pensez à signaler cette erreur."

#: data/resources/ui/error_dialog.blp:45
msgid "Report"
msgstr "Signaler"

#: data/resources/ui/link_window.blp:20 data/resources/ui/link_window.blp:28
#: data/resources/ui/link_window.blp:81
msgid "Link device"
msgstr "Lier l’appareil"

#: data/resources/ui/link_window.blp:33
msgid "Scan Code"
msgstr "Scanner le code"

#: data/resources/ui/link_window.blp:34
msgid "Scan this code with another Signal app logged into your account."
msgstr ""
"Scannez ce code avec une autre application Signal connectée à votre compte."

#: data/resources/ui/link_window.blp:58
msgid "Link without scanning"
msgstr "Lier sans scan"

#: data/resources/ui/link_window.blp:64
msgid ""
"Note: Currently, to sucessfully link contacts requires intervention. Please "
"follow these steps: \n"
"- Link Flare as usual. Wait ~1min after the link window closed. \n"
"- Make sure you do not have running in the background enabled. Close and "
"restart Flare. \n"
"- In the primary menu, click 'synchronize contacts'. Wait ~1min. \n"
"- Make sure you do not have running in the background enabled. Close and "
"restart Flare. \n"
"- Your contacts should be behind the pencil-icon in the top-left."
msgstr ""
"Remarque : actuellement, une intervention est nécessaire pour relier les "
"contacts avec succès. Veuillez suivre les étapes suivantes : \n"
"- Liez Flare comme d’habitude. Attendez ~1min après la fermeture de la "
"fenêtre de liaison. \n"
"- Assurez-vous que vous n’avez pas activé l’exécution en arrière-plan. "
"Fermez et redémarrez Flare. \n"
"- Dans le menu principal, cliquez sur « synchroniser les contacts ». "
"Attendez environ 1 minute. \n"
"- Assurez-vous que vous n’avez pas activé la synchronisation en arrière-"
"plan. Fermez et redémarrez Flare. \n"
"- Vos contacts devraient se trouver derrière l’icône crayon en haut à gauche."

#: data/resources/ui/link_window.blp:74
msgid "Manual linking"
msgstr "Liaison manuelle"

#: data/resources/ui/link_window.blp:86
msgid "Manual Linking"
msgstr "Liaison manuelle"

#: data/resources/ui/link_window.blp:87
msgid ""
"If your device can't scan the QR code, you can manually enter the link "
"provided here"
msgstr ""
"Si votre appareil ne peut scanner le QR code, vous pouvez entrer "
"manuellement le lien fourni ici"

#: data/resources/ui/link_window.blp:98
msgid "Copy to clipboard"
msgstr "Copier vers le presse-papiers"

#: data/resources/ui/message_item.blp:12
msgid "Reply"
msgstr "Répondre"

#: data/resources/ui/message_item.blp:19
msgid "Delete"
msgstr "Supprimer"

#: data/resources/ui/message_item.blp:27
msgid "Copy"
msgstr "Copier"

#: data/resources/ui/message_item.blp:36
msgid "Save as…"
msgstr "Enregistrer sous…"

#: data/resources/ui/message_item.blp:44
msgid "Open with…"
msgstr "Ouvrir avec…"

#: data/resources/ui/preferences_window.blp:6
msgid "General"
msgstr "Général"

#: data/resources/ui/preferences_window.blp:10
msgid "Linking"
msgstr "Liaison"

#: data/resources/ui/preferences_window.blp:11
msgid "The device will need to be relinked to take effect."
msgstr ""
"L’appareil devra être reconnecté pour que les changements prennent effet."

#: data/resources/ui/preferences_window.blp:14
msgid "Device Name"
msgstr "Nom de l’appareil"

#. Translators: Description of Flare in metainfo: Features
#: data/resources/ui/preferences_window.blp:19
#: data/de.schmidhuberj.Flare.metainfo.xml:51
msgid "Attachments"
msgstr "Pièces-jointes"

#: data/resources/ui/preferences_window.blp:22
msgid "Dowload images automatically"
msgstr "Télécharger les images automatiquement"

#: data/resources/ui/preferences_window.blp:26
msgid "Dowload videos automatically"
msgstr "Télécharger les vidéos automatiquement"

#: data/resources/ui/preferences_window.blp:30
msgid "Dowload files automatically"
msgstr "Télécharger les fichiers automatiquement"

#: data/resources/ui/preferences_window.blp:34
msgid "Dowload voice messages automatically"
msgstr "Télécharger les messages vocaux automatiquement"

#: data/resources/ui/preferences_window.blp:39
msgid "Notifications"
msgstr "Notifications"

#: data/resources/ui/preferences_window.blp:40
msgid "Notifications for new messages."
msgstr "Notifications pour les nouveaux messages."

#: data/resources/ui/preferences_window.blp:43
#: data/de.schmidhuberj.Flare.gschema.xml:41
msgid "Send notifications"
msgstr "Envoyer des notifications"

#: data/resources/ui/preferences_window.blp:47
msgid "Send notifications on reactions"
msgstr "Envoyer des notifications pour les réactions"

#: data/resources/ui/preferences_window.blp:52 src/gui/preferences_window.rs:29
msgid "Watch for new messages while closed"
msgstr "Surveiller les nouveaux messages pendant la fermeture"

#: data/resources/ui/preferences_window.blp:60
msgid "Mobile Compatibility"
msgstr "Compatibilité avec les mobiles"

#: data/resources/ui/preferences_window.blp:61
msgid ""
"These options may want to be changed for a better experience on touch- and "
"mobile devices. The default values of those preferences are chosen to be "
"useful on desktop devices."
msgstr ""
"Ces options peuvent être modifiées pour une meilleure expérience sur les "
"appareils tactiles et mobiles. Les valeurs par défaut de ces préférences "
"sont choisies pour être utiles sur les appareils de bureau."

#: data/resources/ui/preferences_window.blp:64
msgid "Selectable Message Text"
msgstr "Texte des messages sélectionnable"

#: data/resources/ui/preferences_window.blp:65
msgid "Selectable text can interfere with swipe-gestures on touch-screens."
msgstr ""
"La sélection peut interférer avec les gestes de balayage sur écrans tactiles."

#: data/resources/ui/preferences_window.blp:69
msgid "Press \"Enter\" to send message"
msgstr "Appuyer sur « Entrée » pour envoyer le message"

#: data/resources/ui/preferences_window.blp:70
msgid ""
"It may not be possible to send messages with newlines on touch keyboards not "
"allowing \"Control + Enter\""
msgstr ""
"Il peut être impossible d'envoyer des messages contenant des nouvelles "
"lignes avec des claviers tactiles ne permettant pas «Contrôle + Entrée»"

#: data/resources/ui/shortcuts.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Général"

#: data/resources/ui/shortcuts.blp:14
msgctxt "shortcut window"
msgid "Show shortcuts"
msgstr "Afficher les raccourcis"

#: data/resources/ui/shortcuts.blp:19
msgctxt "shortcut window"
msgid "Go to settings"
msgstr "Ouvrir les paramètres"

#: data/resources/ui/shortcuts.blp:24
msgctxt "shortcut window"
msgid "Go to about page"
msgstr "Afficher la page À propos"

#: data/resources/ui/shortcuts.blp:29
msgctxt "shortcut window"
msgid "Open menu"
msgstr "Ouvrir le menu"

#: data/resources/ui/shortcuts.blp:34
msgctxt "shortcut window"
msgid "Close window"
msgstr "Fermer la fenêtre"

#: data/resources/ui/shortcuts.blp:40
msgctxt "shortcut window"
msgid "Channels"
msgstr "Canaux de discussion"

#: data/resources/ui/shortcuts.blp:43
msgctxt "shortcut window"
msgid "Go to channel 1…9"
msgstr "Aller à la conversation 1...9"

#: data/resources/ui/shortcuts.blp:48
msgctxt "shortcut window"
msgid "Search in channels"
msgstr "Rechercher dans les canaux de discussion"

#: data/resources/ui/shortcuts.blp:53
msgctxt "shortcut window"
msgid "Upload attachment"
msgstr "Téléverser la pièce jointe"

#: data/resources/ui/shortcuts.blp:58
msgctxt "shortcut window"
msgid "Focus the text input box"
msgstr "Focaliser la zone de saisie de texte"

#: data/resources/ui/shortcuts.blp:63
msgctxt "shortcut window"
msgid "Load more messages"
msgstr "Charger plus de messages"

#: data/resources/ui/text_entry.blp:12
msgid "Message"
msgstr "Message"

#: data/resources/ui/text_entry.blp:38
msgctxt "accessibility"
msgid "Insert an emoji"
msgstr "Insérer un émoji"

#: data/resources/ui/text_entry.blp:44
msgctxt "tooltip"
msgid "Insert emoji"
msgstr "Insérer émoji"

#: data/resources/ui/window.blp:25
msgid "Channel list"
msgstr "Liste des conversations"

#: data/resources/ui/window.blp:37
msgctxt "accessibility"
msgid "Add Conversation"
msgstr "Ajouter une conversation"

#: data/resources/ui/window.blp:48 data/resources/ui/window.blp:91
msgctxt "accessibility"
msgid "Menu"
msgstr "Menu"

#: data/resources/ui/window.blp:78
msgid "Chat"
msgstr "Discussion"

#: data/resources/ui/window.blp:113
msgid "Settings"
msgstr "Paramètres"

#: data/resources/ui/window.blp:128
#: data/resources/ui/submit_captcha_dialog.blp:5
msgid "Submit Captcha"
msgstr "Soumettre un Captcha"

#: data/resources/ui/window.blp:133
msgid "Synchronize Contacts"
msgstr "Synchroniser les contacts"

#: data/resources/ui/window.blp:138
msgid "Keybindings"
msgstr "Raccourcis clavier"

#: data/resources/ui/window.blp:143
msgid "About"
msgstr "À propos"

#: data/resources/ui/window.blp:156
msgid "Clear Messages"
msgstr "Effacer les messages"

#. Translators: Do not translate tags around "on this website".
#: data/resources/ui/submit_captcha_dialog.blp:7
msgid ""
"Submit a Captcha challenge. The token can be obtained from the error message "
"that was displayed from Flare. The captcha must be filled out <a "
"href=\"https://signalcaptchas.org/challenge/generate.html\">on this website</"
"a> and the link to open Signal must be pasted to the corresponding entry. "
"Note that the captcha is only valid for about one minute."
msgstr ""
"Soumettre un challenge Captcha. Le jeton peut être obtenu depuis le message "
"d’erreur qui a été affiché par Flare. Le captcha doit être rempli <a "
"href=\"https://signalcaptchas.org/challenge/generate.html\">sur cette page</"
"a> et le lien d’ouverture de Signal doit être collé dans le champ "
"correspondant. Notez que le captcha n’est valide que pendant une minute "
"environ."

#: data/resources/ui/submit_captcha_dialog.blp:13
msgid "Token"
msgstr "Jeton"

#: data/resources/ui/submit_captcha_dialog.blp:16
msgid "Captcha"
msgstr "Captcha"

#: data/resources/ui/submit_captcha_dialog.blp:24
msgid "Submit"
msgstr "Soumettre"

#: src/backend/channel.rs:581
msgid "Note to self"
msgstr "Note à moi-même"

#: src/backend/manager.rs:430
msgid "You"
msgstr "Vous"

#: src/backend/message/call_message.rs:115
msgid "Started calling."
msgstr "Appel commencé."

#: src/backend/message/call_message.rs:116
msgid "Answered a call."
msgstr "A répondu à un appel."

#: src/backend/message/call_message.rs:117
msgid "Hung up."
msgstr "A raccroché."

#: src/backend/message/call_message.rs:118
msgid "Is busy."
msgstr "Occupé."

#. Translators: When receiving a reaction message in a group, this will be the text to format the notification. Do not translate the text in {}.
#: src/backend/message/reaction_message.rs:70
msgid "{sender} reacted {emoji} to a message."
msgstr "{sender} a réagi par {emoji} à un message."

#. Translators: When receiving a reaction message in a 1-to-1 channel (the body, the sender will be in the notification title and is not included in the translation), this will be the text to format the notification. Do not translate the text in {}.
#: src/backend/message/reaction_message.rs:76
msgid "Reacted {emoji} to a message."
msgstr "A réagi par {emoji} à un message."

#: src/backend/message/text_message.rs:369
msgid "Sent an image"
msgid_plural "Sent {} images"
msgstr[0] "A envoyé une image"
msgstr[1] "A envoyé {} images"

#: src/backend/message/text_message.rs:372
msgid "Sent an video"
msgid_plural "Sent {} videos"
msgstr[0] "A envoyé une vidéo"
msgstr[1] "A envoyé {} vidéos"

#: src/backend/message/text_message.rs:376
msgid "Sent a voice message"
msgid_plural "Sent {} voice messages"
msgstr[0] "A envoyé un message vocal"
msgstr[1] "A envoyé {} messages vocaux"

#: src/backend/message/text_message.rs:382
msgid "Sent a file"
msgid_plural "Sent {} files"
msgstr[0] "A envoyé un fichier"
msgstr[1] "A envoyé {} fichiers"

#: src/error.rs:87
msgid "I/O Error."
msgstr "Erreur de saisie/sortie."

#: src/error.rs:92
msgid "There does not seem to be a connection to the internet available."
msgstr "Il semblerait qu’aucune connexion internet ne soit disponible."

#: src/error.rs:97
msgid "Something glib-related failed."
msgstr "Quelque chose lié à glib a échoué."

#: src/error.rs:102
msgid "The communication with Libsecret failed."
msgstr "La communication avec Libsecret a échoué."

#: src/error.rs:107
msgid ""
"The backend database failed. Please restart the application or delete the "
"database and relink the application."
msgstr ""
"La base de données backend a échoué. Veuillez redémarrer l’application ou "
"supprimer la base de données et lier à nouveau l’application."

#: src/error.rs:112
msgid ""
"You do not seem to be authorized with Signal. Please delete the database and "
"relink the application."
msgstr ""
"Vous semblez ne pas être autorisé par Signal. Veuillez supprimer la base de "
"données et lier à nouveau l’application."

#: src/error.rs:117
msgid "Sending a message failed."
msgstr "Échec de l’envoi d’un message."

#: src/error.rs:122
msgid "Receiving a message failed."
msgstr "Échec de la réception d’un message."

#: src/error.rs:127
msgid ""
"Something unexpected happened with the signal backend. Please retry later."
msgstr ""
"Quelque chose d’inattendu s’est produit avec le backend Signal. Veuillez "
"réessayer plus tard."

#: src/error.rs:132
msgid "The application seems to be misconfigured."
msgstr "L’application semble être mal configurée."

#: src/error.rs:137
msgid "A part of the application crashed."
msgstr "Une partie de l’application a planté."

#: src/error.rs:147
msgid "Please check your internet connection."
msgstr "Veuillez vérifier votre connexion internet."

#: src/error.rs:152
msgid "Please delete the database and relink the device."
msgstr "Veuillez supprimer la base de données et lier à nouveau l’appareil."

#: src/error.rs:159
msgid "The database path at {} is no folder."
msgstr "Le chemin de base de données à {} n’est pas un dossier."

#: src/error.rs:164
msgid "Please restart the application with logging and report this issue."
msgstr ""
"Veuillez redémarrer l’application avec la journalisation activée et signaler "
"ce problème."

#: src/gui/link_window.rs:67
msgid "Copied to clipboard"
msgstr "Copié vers le presse-papiers"

#: src/gui/preferences_window.rs:52
msgid "Background permission"
msgstr "Autorisation d’arrière-plan"

#: src/gui/preferences_window.rs:53
msgid "Use settings to remove permissions"
msgstr "Utiliser les paramètres pour retirer les permissions"

#. How to format time. Should probably be %H:%M (meaning print hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:88 src/gui/utility.rs:112
msgid "%H:%M"
msgstr "%H:%M"

#. How to format a date with time. Should probably be similar to %Y-%m-%d %H:%M (meaning print year, month from 01-12, day from 01-31 (each separated by -), hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:91
msgid "%Y-%m-%d %H:%M"
msgstr "%d/%m/%Y %H:%M"

#: src/gui/utility.rs:124
msgid "Today"
msgstr "Aujourd’hui"

#: src/gui/utility.rs:126
msgid "Yesterday"
msgstr "Hier"

#. How to format a human-readable date including the year. Should probably be similar to %Y-%m-%d (meaning print year, month from 01-12, day from 01-31 (each separated by -)).
#. For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:132
msgid "%Y-%m-%d"
msgstr "%d-%m-%Y"

#. How to format a human-readable date excluding the year. Should probably be similar to "%a., %d. %b" (meaning print abbreviated weekday, day from 1-31 and abbreviated month name).
#. For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:136
msgid "%a., %e. %b"
msgstr "%a., %e. %b"

#: src/gui/window.rs:283 src/gui/window.rs:289
msgid "Remove Messages"
msgstr "Supprimer les messages"

#: src/gui/window.rs:284
msgid "This will remove all locally stored messages from this channel"
msgstr ""
"Ceci effacera tous les messages stockés localement et de cette conversation"

#. Translators: Flare name in metainfo.
#: data/de.schmidhuberj.Flare.desktop.in.in:5
#: data/de.schmidhuberj.Flare.metainfo.xml:8
msgid "Flare"
msgstr "Flare"

#. Translators: Flare summary in metainfo.
#: data/de.schmidhuberj.Flare.desktop.in.in:6
#: data/de.schmidhuberj.Flare.metainfo.xml:10
msgid "Chat with your friends on Signal"
msgstr "Discutez avec vos amis sur Signal"

#. Translators: Description of Flare in metainfo.
#: data/de.schmidhuberj.Flare.metainfo.xml:26
msgid ""
"Flare is an unofficial app that lets you chat with your friends on Signal "
"from Linux. It is still in development and doesn't include as many features "
"as the official Signal applications."
msgstr ""
"Flare est une application non officielle vous permettant de discuter avec "
"vos amis sur Signal depuis Linux. Elle est encore en cours de développement "
"et n’inclut pas toutes les fonctionnalités des applications Signal "
"officielles."

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:31
msgid "Linking device"
msgstr "Liaison de l’appareil"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:35
msgid "Sending messages"
msgstr "Envoi des messages"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:39
msgid "Receiving messages"
msgstr "Réception de messages"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:43
msgid "Replying to a message"
msgstr "Réponse à un message"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:47
msgid "Reacting to a message"
msgstr "Réaction à un message"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:55
msgid "Encrypted storage"
msgstr "Stockage chiffré"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:59
msgid "Notifications, optionally in the background"
msgstr ""
"Notifications, optionnellement quand l’appli fonctionne en arrière-plan"

#. Translators: Description of Flare in metainfo: Security note
#: data/de.schmidhuberj.Flare.metainfo.xml:64
msgid ""
"More features are planned, blocked, or not-planned. Consult the README for "
"more information about them."
msgstr ""
"Plus de fonctionnalités sont prévues, bloquées, ou non envisagées. Consultez "
"le fichier README pour plus d’informations."

#. Translators: Description of Flare in metainfo: Security note
#: data/de.schmidhuberj.Flare.metainfo.xml:68
msgid ""
"Please note that using this application will probably worsen your security "
"compared to using official Signal applications. Use with care when handling "
"sensitive data. Look at the projects README for more information about "
"security."
msgstr ""
"Veuillez noter que l’utilisation de cette application appauvrira sans doute "
"votre sécurité en comparaison avec l’application Signal officielle. Soyez "
"prudent lorsque vous manipulez des données sensibles. Lisez le fichier "
"README du projet pour plus d’informations sur la sécurité."

#: data/de.schmidhuberj.Flare.metainfo.xml:499
msgid "Overview of the application"
msgstr "Aperçu de l’application"

#: data/de.schmidhuberj.Flare.gschema.xml:6
msgid "Window width"
msgstr "Largeur de la fenêtre"

#: data/de.schmidhuberj.Flare.gschema.xml:10
msgid "Window height"
msgstr "Hauteur de la fenêtre"

#: data/de.schmidhuberj.Flare.gschema.xml:14
msgid "Window maximized state"
msgstr "Fenêtre maximisée"

#: data/de.schmidhuberj.Flare.gschema.xml:19
msgid ""
"The device name when linking Flare. This requires the application to be re-"
"linked."
msgstr ""
"Le nom de l’appareil lors de la liaison de Flare. Ceci nécessite que "
"l’application soit à nouveau liée."

#: data/de.schmidhuberj.Flare.gschema.xml:24
msgid "Automatically download images"
msgstr "Télécharger automatiquement les images"

#: data/de.schmidhuberj.Flare.gschema.xml:28
msgid "Automatically download videos"
msgstr "Télécharger automatiquement les vidéos"

#: data/de.schmidhuberj.Flare.gschema.xml:32
msgid "Automatically download voice messages"
msgstr "Télécharger automatiquement les messages vocaux"

#: data/de.schmidhuberj.Flare.gschema.xml:36
msgid "Automatically download files"
msgstr "Télécharger automatiquement les fichiers"

#: data/de.schmidhuberj.Flare.gschema.xml:45
msgid "Run in background when closed"
msgstr "Exécuter en arrière-plan à la fermeture"

#: data/de.schmidhuberj.Flare.gschema.xml:49
msgid "Send notifications when retrieving reactions"
msgstr "Envoyer des notifications à la réception de réactions"

#: data/de.schmidhuberj.Flare.gschema.xml:54
msgid "Can messages be selected"
msgstr "Sélection des messages possible"

#: data/de.schmidhuberj.Flare.gschema.xml:58
msgid "Send a message when the Enter-key is pressed"
msgstr "Envoyer un message quand la touche « Entrée » est appuyée"

#~ msgid "Send"
#~ msgstr "Envoyer"

#~ msgid "Load media"
#~ msgstr "Charger le média"

#~ msgid "Load image"
#~ msgstr "Charger l’image"

#~ msgid "Load video"
#~ msgstr "Charger la vidéo"

#~ msgid "Load file"
#~ msgstr "Charger le fichier"

#~ msgid "Load voice message"
#~ msgstr "Charger le message vocal"

#~ msgid "Link Device"
#~ msgstr "Lier l’appareil"

#~ msgid "Please scan this QR code with your primary device."
#~ msgstr "Veuillez scanner ce code QR avec votre appareil principal."

#~ msgid "Please wait until the contacts are displayed after linking."
#~ msgstr "Veuillez attendre que les contacts s’affichent après la liaison."

#~ msgid "Signal GTK Client"
#~ msgstr "Un client GTK pour Signal"

#~ msgid "An unofficial Signal GTK client"
#~ msgstr "Un client GTK non officiel pour Signal"

#~ msgid ""
#~ "An unofficial GTK client for the messaging application Signal. This is a "
#~ "very simple application with many features missing compared to the "
#~ "official applications."
#~ msgstr ""
#~ "Un client GTK non officiel pour l’application de messagerie Signal. C’est "
#~ "une application très simple avec un certain nombre de fonctionnalités en "
#~ "moins par rapport à l’application officielle."

#~ msgid "Sending and receiving attachments"
#~ msgstr "Envoyer et recevoir des pièces-jointes"

#~ msgid "Other"
#~ msgstr "Autre"

#~ msgctxt "accessibility"
#~ msgid "Download"
#~ msgstr "Télécharger"

#~ msgid "React"
#~ msgstr "Réagir"

#~ msgctxt "accessibility"
#~ msgid "Reset Identity"
#~ msgstr "Réinitialiser l’identité"

#~ msgid "Reset Identity"
#~ msgstr "Réinitialiser l’identité"

#~ msgid "Message Storage"
#~ msgstr "Stockage des messages"

#~ msgid "Number of messages to load on startup"
#~ msgstr "Nombre de messages à charger au démarrage"

#~ msgid "Per channel"
#~ msgstr "Par chaîne"

#~ msgid "Number of messages to load on request"
#~ msgstr "Nombre de messages à charger sur demande"

#~ msgid "The amount of messages to load on startup for each channel"
#~ msgstr "Nombre de messages à charger au démarrage pour chaque canal"

#~ msgid "The amount of messages to load on request for each channel"
#~ msgstr "Nombre de messages à charger sur demande pour chaque canal"

#, fuzzy
#~ msgid "Changed"
#~ msgstr "Chaînes"

#, fuzzy
#~ msgid "Unlink without deleting messages."
#~ msgstr "Délier mais garder les messages"

#, fuzzy
#~ msgid "Messages and notifications for calls."
#~ msgstr "Envoyer des notifications"

#, fuzzy
#~ msgid "Greatly refactored messages."
#~ msgstr "Effacer les messages"

#, fuzzy
#~ msgid "Changed:"
#~ msgstr "Chaînes"

#, fuzzy
#~ msgid "Notification support."
#~ msgstr "Notifications"

#, fuzzy
#~ msgid "New message storage backend."
#~ msgstr "Stockage des messages"

#, fuzzy
#~ msgid "Search for the channel view."
#~ msgstr "Rechercher dans les chaînes"

#, fuzzy
#~ msgid "Message storage."
#~ msgstr "Stockage des messages"

#, fuzzy
#~ msgid "Keyboard shortcuts."
#~ msgstr "Afficher les raccourcis"

#~ msgid ""
#~ "Notifications for new messages. These notifications will only work while "
#~ "Flare is open, if you also want to be notified when this application is "
#~ "closed, consider using something like messenger-notify."
#~ msgstr ""
#~ "Notifications pour les nouveaux messages. Ces notifications ne "
#~ "fonctionneront que pendant que Flare est ouvert, si vous souhaitez être "
#~ "également notifié⋅e quand l’application est fermée, envisagez d’utiliser "
#~ "messenger-notify."

#~ msgid "Are you sure?"
#~ msgstr "Êtes-vous sûr·e ?"
