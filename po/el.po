# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the flare package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: flare\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-09 13:04+0200\n"
"PO-Revision-Date: 2023-09-01 18:55+0000\n"
"Last-Translator: yiannis ioannides <sub@wai.ai>\n"
"Language-Team: Greek <https://hosted.weblate.org/projects/flare/flare/el/>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.0.1-dev\n"

#: data/resources/ui/about.blp.in:13
msgid "translator-credits"
msgstr "Yiannis Ioannides"

#: data/resources/ui/channel_info_dialog.blp:5 data/resources/ui/window.blp:152
msgid "Channel Information"
msgstr "Πληροφορίες καναλιού"

#: data/resources/ui/channel_info_dialog.blp:28
msgctxt "accessibility"
msgid "Reset Session"
msgstr "Επαναφορά συνεδρίας"

#: data/resources/ui/channel_info_dialog.blp:31
msgid "Reset Session"
msgstr "Επαναφορά συνεδρίας"

#: data/resources/ui/channel_info_dialog.blp:40
#: data/resources/ui/error_dialog.blp:44
#: data/resources/ui/submit_captcha_dialog.blp:23
msgid "Close"
msgstr "Κλείσιμο"

#: data/resources/ui/channel_list.blp:11
#: data/resources/ui/channel_messages.blp:15
msgid "Offline"
msgstr "Εκτός σύνδεσης"

#: data/resources/ui/channel_list.blp:19
msgid "Search"
msgstr "Αναζήτηση"

#: data/resources/ui/channel_list.blp:22 data/resources/ui/window.blp:60
msgctxt "accessibility"
msgid "Search"
msgstr "Αναζήτηση"

#: data/resources/ui/channel_messages.blp:21
msgid "No Channel Selected"
msgstr "Χωρίς επιλεγμένο κανάλι"

#: data/resources/ui/channel_messages.blp:22
msgid "Select a channel"
msgstr "Επιλέξτε κανάλι"

#: data/resources/ui/channel_messages.blp:91
msgctxt "accessibility"
msgid "Remove the reply"
msgstr "Αφαιρέστε την απάντηση"

#: data/resources/ui/channel_messages.blp:94
msgctxt "tooltip"
msgid "Remove reply"
msgstr "Αφαίρεση απάντησης"

#: data/resources/ui/channel_messages.blp:119
msgctxt "accessibility"
msgid "Remove an attachment"
msgstr "Αφαιρέστε ένα συνημμένο"

#: data/resources/ui/channel_messages.blp:122
msgctxt "tooltip"
msgid "Remove attachment"
msgstr "Αφαίρεση συνημμένου"

#: data/resources/ui/channel_messages.blp:153
msgctxt "accessibility"
msgid "Add an attachment"
msgstr "Προσθέστε ένα συνημμένο"

#: data/resources/ui/channel_messages.blp:160
msgctxt "tooltip"
msgid "Add attachment"
msgstr "Πρόσθεση συνημμένου"

#: data/resources/ui/channel_messages.blp:176
msgctxt "accessibility"
msgid "Message input"
msgstr "Προσθήκη μηνύματος"

#: data/resources/ui/channel_messages.blp:182
msgctxt "tooltip"
msgid "Message input"
msgstr "Προσθήκη μηνύματος"

#: data/resources/ui/channel_messages.blp:192
msgctxt "accessibility"
msgid "Send"
msgstr "Αποστολή"

#: data/resources/ui/channel_messages.blp:195
msgctxt "tooltip"
msgid "Send"
msgstr "Αποστολή"

#: data/resources/ui/dialog_clear_messages.blp:5
#: data/resources/ui/window.blp:123
msgid "Clear messages"
msgstr "Καθαρισμός μηνυμάτων"

#: data/resources/ui/dialog_clear_messages.blp:6
msgid ""
"This will clear all locally stored messages. This will also close the "
"application."
msgstr ""
"Έχετε επιλέξει το καθαρισμό όλων των αποθηκευμένων μηνυμάτων. Αυτή η επιλογή "
"θα κλείσει επίσης την εφαρμογή."

#: data/resources/ui/dialog_clear_messages.blp:11
#: data/resources/ui/dialog_unlink.blp:11 src/gui/window.rs:288
msgid "Cancel"
msgstr "Ακύρωση"

#: data/resources/ui/dialog_clear_messages.blp:12
msgid "Clear"
msgstr "Καθαρισμός"

#: data/resources/ui/dialog_unlink.blp:5 data/resources/ui/window.blp:118
msgid "Unlink"
msgstr "Αποσύνδεση"

#: data/resources/ui/dialog_unlink.blp:6
msgid ""
"This will unlink this device and remove all locally saved data. This will "
"also close the application such that it can be relinked when opening it "
"again."
msgstr ""
"Έχετε επιλέξει την αποσύνδεση της συσκευής και αφαίρεση όλων των "
"αποθηκευμένων δεδομένων. Αυτή η επιλογή θα κλείσει επίσης την εφαρμογή, με "
"τη δυνατότητα επανασύνδεσης κατόπιν επανεκκίνησης της εφαρμογής."

#: data/resources/ui/dialog_unlink.blp:12
msgid "Unlink and delete all messages"
msgstr "Αποσύνδεση και διαγραφή όλων των μηνυμάτων"

#: data/resources/ui/dialog_unlink.blp:13
msgid "Unlink but keep messages"
msgstr "Αποσύνδεση χωρίς διαγραφή μηνυμάτων"

#: data/resources/ui/error_dialog.blp:5
msgid "Error"
msgstr "Σφάλμα"

#: data/resources/ui/error_dialog.blp:26
msgid "Please consider reporting this error."
msgstr "Παρακαλώ αναφέρετε αυτό το σφάλμα."

#: data/resources/ui/error_dialog.blp:45
msgid "Report"
msgstr "Αναφορά"

#: data/resources/ui/link_window.blp:20 data/resources/ui/link_window.blp:28
#: data/resources/ui/link_window.blp:81
msgid "Link device"
msgstr "Σύνδεση συσκευής"

#: data/resources/ui/link_window.blp:33
msgid "Scan Code"
msgstr "Ανίχνευση κωδικού"

#: data/resources/ui/link_window.blp:34
msgid "Scan this code with another Signal app logged into your account."
msgstr "Ανιχνεύστε τον κωδικό με κάποια άλλη ήδη συνδεδεμένη εφαρμογή Signal."

#: data/resources/ui/link_window.blp:58
msgid "Link without scanning"
msgstr "Σύνδεσμος χωρίς ανίχνευση"

#: data/resources/ui/link_window.blp:64
msgid ""
"Note: Currently, to sucessfully link contacts requires intervention. Please "
"follow these steps: \n"
"- Link Flare as usual. Wait ~1min after the link window closed. \n"
"- Make sure you do not have running in the background enabled. Close and "
"restart Flare. \n"
"- In the primary menu, click 'synchronize contacts'. Wait ~1min. \n"
"- Make sure you do not have running in the background enabled. Close and "
"restart Flare. \n"
"- Your contacts should be behind the pencil-icon in the top-left."
msgstr ""

#: data/resources/ui/link_window.blp:74
#, fuzzy
msgid "Manual linking"
msgstr "Χειρωνακτική Σύνδεση"

#: data/resources/ui/link_window.blp:86
msgid "Manual Linking"
msgstr "Χειρωνακτική Σύνδεση"

#: data/resources/ui/link_window.blp:87
msgid ""
"If your device can't scan the QR code, you can manually enter the link "
"provided here"
msgstr ""
"Αν η συσκευή σαν δεν μπορεί να ανιχνεύσει τον QR κωδικό, μπορείτε να "
"εισάγετε μόνοι σας εδώ τον σύνδεσμο που παρέχεται"

#: data/resources/ui/link_window.blp:98
msgid "Copy to clipboard"
msgstr "Αντιγραφή στο πρόχειρο"

#: data/resources/ui/message_item.blp:12
msgid "Reply"
msgstr "Απάντηση"

#: data/resources/ui/message_item.blp:19
msgid "Delete"
msgstr "Διαγραφή"

#: data/resources/ui/message_item.blp:27
msgid "Copy"
msgstr "Αντιγραφή"

#: data/resources/ui/message_item.blp:36
msgid "Save as…"
msgstr "Αποθήκευση ως…"

#: data/resources/ui/message_item.blp:44
msgid "Open with…"
msgstr "Άνοιγμα με…"

#: data/resources/ui/preferences_window.blp:6
msgid "General"
msgstr "Γενικά"

#: data/resources/ui/preferences_window.blp:10
msgid "Linking"
msgstr "Σύνδεση"

#: data/resources/ui/preferences_window.blp:11
msgid "The device will need to be relinked to take effect."
msgstr "Η συσκευή πρέπει να επανασυνδεθεί για να ολοκληρωθεί η διαδικασία."

#: data/resources/ui/preferences_window.blp:14
msgid "Device Name"
msgstr "Όνομα συσκευής"

#. Translators: Description of Flare in metainfo: Features
#: data/resources/ui/preferences_window.blp:19
#: data/de.schmidhuberj.Flare.metainfo.xml:51
msgid "Attachments"
msgstr "Συνημμένα"

#: data/resources/ui/preferences_window.blp:22
msgid "Dowload images automatically"
msgstr "Αυτόματη λήψη εικόνων"

#: data/resources/ui/preferences_window.blp:26
msgid "Dowload videos automatically"
msgstr "Αυτόματη λήψη βίντεο"

#: data/resources/ui/preferences_window.blp:30
msgid "Dowload files automatically"
msgstr "Αυτόματη λήψη αρχείων"

#: data/resources/ui/preferences_window.blp:34
msgid "Dowload voice messages automatically"
msgstr "Αυτόματη λήψη φωνητικών μηνυμάτων"

#: data/resources/ui/preferences_window.blp:39
msgid "Notifications"
msgstr "Γνωστοποιήσεις"

#: data/resources/ui/preferences_window.blp:40
msgid "Notifications for new messages."
msgstr "Γνωστοποιήσεις καινούργιων μηνυμάτων."

#: data/resources/ui/preferences_window.blp:43
#: data/de.schmidhuberj.Flare.gschema.xml:41
msgid "Send notifications"
msgstr "Αποστολή γνωστοποιήσεων"

#: data/resources/ui/preferences_window.blp:47
msgid "Send notifications on reactions"
msgstr "Αποστολή ειδοποιήσεων σχετικά με αντιδράσεις"

#: data/resources/ui/preferences_window.blp:52 src/gui/preferences_window.rs:29
msgid "Watch for new messages while closed"
msgstr "Αναμονή καινούργιων μηνυμάτων κατά το κλείσιμο του παραθύρου"

#: data/resources/ui/preferences_window.blp:60
msgid "Mobile Compatibility"
msgstr "Συμβατότητα κινητού"

#: data/resources/ui/preferences_window.blp:61
msgid ""
"These options may want to be changed for a better experience on touch- and "
"mobile devices. The default values of those preferences are chosen to be "
"useful on desktop devices."
msgstr ""
"Μπορείτε να αλλάξετε τις ρυθμίσεις για μια καλύτερη εμπειρία σε κινητές ή "
"touch συσκευές. Οι προεπιλεγμένες ρυθμίσεις είναι ορισμένες με βάση τη "
"καλύτερη λειτουργία σε συσκευές desktop."

#: data/resources/ui/preferences_window.blp:64
msgid "Selectable Message Text"
msgstr "Επιλέξιμο κείμενο μηνύματος"

#: data/resources/ui/preferences_window.blp:65
msgid "Selectable text can interfere with swipe-gestures on touch-screens."
msgstr ""
"Επιλέξιμο κείμενο μπορεί να παρέμβει στα swipe gestures σε οθόνες αφής."

#: data/resources/ui/preferences_window.blp:69
msgid "Press \"Enter\" to send message"
msgstr "Πατήστε \"Enter\" για αποστολή μηνύματος"

#: data/resources/ui/preferences_window.blp:70
msgid ""
"It may not be possible to send messages with newlines on touch keyboards not "
"allowing \"Control + Enter\""
msgstr ""
"Μπορεί να μην είναι δυνατή η αποστολή μηνυμάτων με νέες γραμμές σε πλήκτρα "
"αφής που δεν επιτρέπουν το συνδυασμό \"Control + Enter\""

#: data/resources/ui/shortcuts.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Γενικά"

#: data/resources/ui/shortcuts.blp:14
msgctxt "shortcut window"
msgid "Show shortcuts"
msgstr "Εμφάνιση συντομεύσεων"

#: data/resources/ui/shortcuts.blp:19
msgctxt "shortcut window"
msgid "Go to settings"
msgstr "Προς Ρυθμίσεις"

#: data/resources/ui/shortcuts.blp:24
msgctxt "shortcut window"
msgid "Go to about page"
msgstr "Προς Γενικά"

#: data/resources/ui/shortcuts.blp:29
msgctxt "shortcut window"
msgid "Open menu"
msgstr "Άνοιγμα μενού"

#: data/resources/ui/shortcuts.blp:34
msgctxt "shortcut window"
msgid "Close window"
msgstr "Κλείσιμο παραθύρου"

#: data/resources/ui/shortcuts.blp:40
msgctxt "shortcut window"
msgid "Channels"
msgstr "Κανάλια"

#: data/resources/ui/shortcuts.blp:43
msgctxt "shortcut window"
msgid "Go to channel 1…9"
msgstr "Προς κανάλι 1...9"

#: data/resources/ui/shortcuts.blp:48
msgctxt "shortcut window"
msgid "Search in channels"
msgstr "Αναζήτηση στα κανάλια"

#: data/resources/ui/shortcuts.blp:53
msgctxt "shortcut window"
msgid "Upload attachment"
msgstr "Αναφόρτωση συνημμένου"

#: data/resources/ui/shortcuts.blp:58
msgctxt "shortcut window"
msgid "Focus the text input box"
msgstr "Εστίαση στο πλαίσιο εισαγωγής κειμένου"

#: data/resources/ui/shortcuts.blp:63
msgctxt "shortcut window"
msgid "Load more messages"
msgstr "Λήψη περισσότερων μηνυμάτων"

#: data/resources/ui/text_entry.blp:12
#, fuzzy
msgid "Message"
msgstr "Προσθήκη μηνύματος"

#: data/resources/ui/text_entry.blp:38
msgctxt "accessibility"
msgid "Insert an emoji"
msgstr "Εισάγετε ένα emoji"

#: data/resources/ui/text_entry.blp:44
msgctxt "tooltip"
msgid "Insert emoji"
msgstr "Εισαγωγή emoji"

#: data/resources/ui/window.blp:25
#, fuzzy
msgid "Channel list"
msgstr "Κανάλια"

#: data/resources/ui/window.blp:37
msgctxt "accessibility"
msgid "Add Conversation"
msgstr "Προσθήκη συνομιλίας"

#: data/resources/ui/window.blp:48 data/resources/ui/window.blp:91
msgctxt "accessibility"
msgid "Menu"
msgstr "Μενού"

#: data/resources/ui/window.blp:78
msgid "Chat"
msgstr ""

#: data/resources/ui/window.blp:113
msgid "Settings"
msgstr "Ρυθμίσεις"

#: data/resources/ui/window.blp:128
#: data/resources/ui/submit_captcha_dialog.blp:5
msgid "Submit Captcha"
msgstr "Υποβολή captcha"

#: data/resources/ui/window.blp:133
msgid "Synchronize Contacts"
msgstr ""

#: data/resources/ui/window.blp:138
msgid "Keybindings"
msgstr "Δεσμοί κλειδιών"

#: data/resources/ui/window.blp:143
msgid "About"
msgstr "Σχετικά"

#: data/resources/ui/window.blp:156
#, fuzzy
msgid "Clear Messages"
msgstr "Καθαρισμός μηνυμάτων"

#. Translators: Do not translate tags around "on this website".
#: data/resources/ui/submit_captcha_dialog.blp:7
msgid ""
"Submit a Captcha challenge. The token can be obtained from the error message "
"that was displayed from Flare. The captcha must be filled out <a "
"href=\"https://signalcaptchas.org/challenge/generate.html\">on this website</"
"a> and the link to open Signal must be pasted to the corresponding entry. "
"Note that the captcha is only valid for about one minute."
msgstr ""
"Υποβολή captcha: το token μπορεί να αποκτηθεί από το μήνυμα σφάλματος που "
"προβλήθηκε από το Flare. Το captcha πρέπει να συμπληρωθεί <a href=\"https://"
"signalcaptchas.org/challenge/generate.html\">σε αυτή τη σελίδα</a> και το "
"link για να ανοίξετε το Signal να επικολληθεί στο αντίστοιχο πλαίσιο. "
"Σημειώστε ότι το captcha ισχύει μόνο για ένα λεπτό."

#: data/resources/ui/submit_captcha_dialog.blp:13
msgid "Token"
msgstr "Token"

#: data/resources/ui/submit_captcha_dialog.blp:16
msgid "Captcha"
msgstr "Captcha"

#: data/resources/ui/submit_captcha_dialog.blp:24
msgid "Submit"
msgstr "Υποβολή"

#: src/backend/channel.rs:581
msgid "Note to self"
msgstr "Σημειώσεις Προς Εμένα"

#: src/backend/manager.rs:430
msgid "You"
msgstr "Εσείς"

#: src/backend/message/call_message.rs:115
msgid "Started calling."
msgstr "Έναρξη κλήσης."

#: src/backend/message/call_message.rs:116
msgid "Answered a call."
msgstr "Απάντηση κλήσης."

#: src/backend/message/call_message.rs:117
msgid "Hung up."
msgstr "Λήξη κλήσης."

#: src/backend/message/call_message.rs:118
msgid "Is busy."
msgstr "Κατειλημμένο."

#. Translators: When receiving a reaction message in a group, this will be the text to format the notification. Do not translate the text in {}.
#: src/backend/message/reaction_message.rs:70
msgid "{sender} reacted {emoji} to a message."
msgstr "Ο/Η {sender} αντέδρασε με {emoji} σε ένα μήνυμα."

#. Translators: When receiving a reaction message in a 1-to-1 channel (the body, the sender will be in the notification title and is not included in the translation), this will be the text to format the notification. Do not translate the text in {}.
#: src/backend/message/reaction_message.rs:76
msgid "Reacted {emoji} to a message."
msgstr "Αντέδρασε με {emoji} σε ένα μήνυμα."

#: src/backend/message/text_message.rs:369
msgid "Sent an image"
msgid_plural "Sent {} images"
msgstr[0] "Έστειλε μια εικόνα"
msgstr[1] "Έστειλε {} εικόνες"

#: src/backend/message/text_message.rs:372
msgid "Sent an video"
msgid_plural "Sent {} videos"
msgstr[0] "Έστειλε ένα βίντεο"
msgstr[1] "Έστειλε {} βίντεο"

#: src/backend/message/text_message.rs:376
msgid "Sent a voice message"
msgid_plural "Sent {} voice messages"
msgstr[0] "Έστειλε ένα φωνητικό μήνυμα"
msgstr[1] "Έστειλε {} φωνητικά μηνύματα"

#: src/backend/message/text_message.rs:382
msgid "Sent a file"
msgid_plural "Sent {} files"
msgstr[0] "Έστειλε ένα αρχείο"
msgstr[1] "Έστειλε {} αρχεία"

#: src/error.rs:87
msgid "I/O Error."
msgstr "Σφάλμα εισόδου-εξόδου."

#: src/error.rs:92
msgid "There does not seem to be a connection to the internet available."
msgstr "Δεν υπάρχει διαθέσιμη σύνδεση στο διαδίκτυο."

#: src/error.rs:97
msgid "Something glib-related failed."
msgstr "Σφάλμα σχετικό με το πρωτόκολλο glib."

#: src/error.rs:102
msgid "The communication with Libsecret failed."
msgstr "Σφάλμα επικοινωνίας με το πρωτόκολλο Libsecret."

#: src/error.rs:107
msgid ""
"The backend database failed. Please restart the application or delete the "
"database and relink the application."
msgstr ""
"Η βάση δεδομένων backend απέτυχε. Παρακαλώ επανεκκινήστε την εφαρμογή ή "
"διαγράψτε τη βάση δεδομένων και συνδέστε ξανά την εφαρμογή."

#: src/error.rs:112
msgid ""
"You do not seem to be authorized with Signal. Please delete the database and "
"relink the application."
msgstr ""
"Δεν φαίνεται να είστε εξουσιοδοτημένος με το Signal. Διαγράψτε τη βάση "
"δεδομένων και συνδέστε ξανά την εφαρμογή."

#: src/error.rs:117
msgid "Sending a message failed."
msgstr "Η αποστολή μηνύματος απέτυχε."

#: src/error.rs:122
msgid "Receiving a message failed."
msgstr "Η λήψη μηνύματος απέτυχε."

#: src/error.rs:127
msgid ""
"Something unexpected happened with the signal backend. Please retry later."
msgstr ""
"Κάτι απροσδόκητο συνέβη με το backend του Signal. Προσπαθήστε ξανά αργότερα."

#: src/error.rs:132
msgid "The application seems to be misconfigured."
msgstr "Η εφαρμογή φαίνεται να είναι λάθος ρυθμισμένη."

#: src/error.rs:137
msgid "A part of the application crashed."
msgstr "Ένα μέρος της εφαρμογής κατέρρευσε."

#: src/error.rs:147
msgid "Please check your internet connection."
msgstr "Παρακαλώ ελέγξτε τη σύνδεση διαδικτύου."

#: src/error.rs:152
msgid "Please delete the database and relink the device."
msgstr "Παρακαλώ διαγράψτε τη βάση δεδομένων και επανασυνδέστε τη συσκευή."

#: src/error.rs:159
msgid "The database path at {} is no folder."
msgstr "Ο προορισμός της βάσης δεδομένων στο {} δεν είναι φάκελος."

#: src/error.rs:164
msgid "Please restart the application with logging and report this issue."
msgstr ""
"Παρακαλώ επανεκκινήστε την εφαρμογή με καταγραφή και αναφέρετε αυτό το "
"πρόβλημα."

#: src/gui/link_window.rs:67
msgid "Copied to clipboard"
msgstr "Αντιγράφτηκε στο πρόχειρο"

#: src/gui/preferences_window.rs:52
msgid "Background permission"
msgstr "Άδεια λειτουργίας στο παρασκήνιο"

#: src/gui/preferences_window.rs:53
msgid "Use settings to remove permissions"
msgstr "Χρήση ρυθμίσεων για την κατάργηση δικαιωμάτων"

#. How to format time. Should probably be %H:%M (meaning print hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:88 src/gui/utility.rs:112
msgid "%H:%M"
msgstr "%H:%M"

#. How to format a date with time. Should probably be similar to %Y-%m-%d %H:%M (meaning print year, month from 01-12, day from 01-31 (each separated by -), hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:91
msgid "%Y-%m-%d %H:%M"
msgstr "%Y-%m-%d %H:%M"

#: src/gui/utility.rs:124
msgid "Today"
msgstr ""

#: src/gui/utility.rs:126
msgid "Yesterday"
msgstr ""

#. How to format a human-readable date including the year. Should probably be similar to %Y-%m-%d (meaning print year, month from 01-12, day from 01-31 (each separated by -)).
#. For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:132
#, fuzzy
msgid "%Y-%m-%d"
msgstr "%Y-%m-%d %H:%M"

#. How to format a human-readable date excluding the year. Should probably be similar to "%a., %d. %b" (meaning print abbreviated weekday, day from 1-31 and abbreviated month name).
#. For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:136
msgid "%a., %e. %b"
msgstr ""

#: src/gui/window.rs:283 src/gui/window.rs:289
#, fuzzy
msgid "Remove Messages"
msgstr "Λήψη μηνυμάτων"

#: src/gui/window.rs:284
#, fuzzy
msgid "This will remove all locally stored messages from this channel"
msgstr ""
"Έχετε επιλέξει το καθαρισμό όλων των αποθηκευμένων μηνυμάτων. Αυτή η επιλογή "
"θα κλείσει επίσης την εφαρμογή."

#. Translators: Flare name in metainfo.
#: data/de.schmidhuberj.Flare.desktop.in.in:5
#: data/de.schmidhuberj.Flare.metainfo.xml:8
msgid "Flare"
msgstr "Flare"

#. Translators: Flare summary in metainfo.
#: data/de.schmidhuberj.Flare.desktop.in.in:6
#: data/de.schmidhuberj.Flare.metainfo.xml:10
msgid "Chat with your friends on Signal"
msgstr "Συνομιλήστε με τους φίλους σας στο Signal"

#. Translators: Description of Flare in metainfo.
#: data/de.schmidhuberj.Flare.metainfo.xml:26
msgid ""
"Flare is an unofficial app that lets you chat with your friends on Signal "
"from Linux. It is still in development and doesn't include as many features "
"as the official Signal applications."
msgstr ""
"Το Flare είναι μια ανεπίσημη εφαρμογή που σας επιτρέπει να συνομιλείτε με "
"τους φίλους σας μέσω Signal στα Linux. Είναι ακόμη υπό ανάπτυξη και δεν "
"συμπεριλαμβάνει κάποια χαρακτηριστικά που έχουν οι επίσημες εφαρμογές."

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:31
msgid "Linking device"
msgstr "Σύνδεση συσκευής"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:35
msgid "Sending messages"
msgstr "Αποστολή μηνυμάτων"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:39
msgid "Receiving messages"
msgstr "Λήψη μηνυμάτων"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:43
msgid "Replying to a message"
msgstr "Απάντηση σε μήνυμα"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:47
msgid "Reacting to a message"
msgstr "Αντίδραση σε μήνυμα"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:55
msgid "Encrypted storage"
msgstr "Κρυπτογραφημένος αποθηκευτικός χώρος"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:59
msgid "Notifications, optionally in the background"
msgstr "Γνωστοποιήσεις, προαιρετικά στο παρασκήνιο"

#. Translators: Description of Flare in metainfo: Security note
#: data/de.schmidhuberj.Flare.metainfo.xml:64
msgid ""
"More features are planned, blocked, or not-planned. Consult the README for "
"more information about them."
msgstr ""
"Περισσότερες λειτουργίες είναι προγραμματισμένες, μπλοκαρισμένες ή μη "
"σχεδιασμένες. Αναφερθείτε στο README για περισσότερες πληροφορίες σχετικά με "
"τα παραπάνω."

#. Translators: Description of Flare in metainfo: Security note
#: data/de.schmidhuberj.Flare.metainfo.xml:68
msgid ""
"Please note that using this application will probably worsen your security "
"compared to using official Signal applications. Use with care when handling "
"sensitive data. Look at the projects README for more information about "
"security."
msgstr ""
"Παρακαλώ λάβετε υπόψη ότι η χρήση αυτής της εφαρμογής πιθανότατα θα "
"επιδεινώσει την ασφάλειά σας σε σύγκριση με τη χρήση επίσημων εφαρμογών "
"Signal. Χρησιμοποιήστε με προσοχή όταν χειρίζεστε ευαίσθητα δεδομένα. "
"Αναφερθείτε στο README για περισσότερες πληροφορίες σχετικά με το θέμα "
"ασφάλειας."

#: data/de.schmidhuberj.Flare.metainfo.xml:499
msgid "Overview of the application"
msgstr "Επισκόπηση της εφαρμογής"

#: data/de.schmidhuberj.Flare.gschema.xml:6
msgid "Window width"
msgstr "Πλάτος παραθύρου"

#: data/de.schmidhuberj.Flare.gschema.xml:10
msgid "Window height"
msgstr "Ύψος παραθύρου"

#: data/de.schmidhuberj.Flare.gschema.xml:14
msgid "Window maximized state"
msgstr "Κατάσταση μεγιστοποιημένου παραθύρου"

#: data/de.schmidhuberj.Flare.gschema.xml:19
msgid ""
"The device name when linking Flare. This requires the application to be re-"
"linked."
msgstr ""
"Το όνομα της συσκευής κατά τη σύνδεση του Flare. Αυτό απαιτεί την εκ νέου "
"σύνδεση της εφαρμογής."

#: data/de.schmidhuberj.Flare.gschema.xml:24
msgid "Automatically download images"
msgstr "Αυτόματη λήψη εικόνων"

#: data/de.schmidhuberj.Flare.gschema.xml:28
msgid "Automatically download videos"
msgstr "Αυτόματη λήψη βίντεο"

#: data/de.schmidhuberj.Flare.gschema.xml:32
msgid "Automatically download voice messages"
msgstr "Αυτόματη λήψη φωνητικών μηνυμάτων"

#: data/de.schmidhuberj.Flare.gschema.xml:36
msgid "Automatically download files"
msgstr "Αυτόματη λήψη αρχείων"

#: data/de.schmidhuberj.Flare.gschema.xml:45
msgid "Run in background when closed"
msgstr "Λειτουργία στο παρασκήνιο όσο το παράθυρο είναι κλειστό"

#: data/de.schmidhuberj.Flare.gschema.xml:49
msgid "Send notifications when retrieving reactions"
msgstr "Αποστολή ειδοποιήσεων σχετικά με αντιδράσεις"

#: data/de.schmidhuberj.Flare.gschema.xml:54
msgid "Can messages be selected"
msgstr "Μηνύματα είναι επιλέξιμα"

#: data/de.schmidhuberj.Flare.gschema.xml:58
msgid "Send a message when the Enter-key is pressed"
msgstr "Στείλτε μήνυμα αφού πατήσετε Enter"

#~ msgid "Send"
#~ msgstr "Αποστολή"

#~ msgid "Load media"
#~ msgstr "Φόρτωση μέσων"

#~ msgid "Signal GTK Client"
#~ msgstr "Ένα ανεπίσημο πρόγραμμα Signal γραμμένο σε GTK"

#, fuzzy
#~ msgid "Changed"
#~ msgstr "Κανάλια"

#, fuzzy
#~ msgid "Notifications on reactions received (configurable)."
#~ msgstr "Αποστολή ειδοποιήσεων σχετικά με αντιδράσεις"

#, fuzzy
#~ msgid "Setting to turn off sending a message when the enter-key is pressed."
#~ msgstr "Στείλτε μήνυμα αφού πατήσετε Enter"

#, fuzzy
#~ msgid "Button to scroll down in the messages view."
#~ msgstr "Αυτόματη λήψη φωνητικών μηνυμάτων"

#, fuzzy
#~ msgid "Message deletion."
#~ msgstr "Προσθήκη μηνύματος"

#, fuzzy
#~ msgid "Updated application icon and emblem."
#~ msgstr "Ένα μέρος της εφαρμογής κατέρρευσε."

#, fuzzy
#~ msgid "Copy action in message menu."
#~ msgstr "Απάντηση σε μήνυμα"

#, fuzzy
#~ msgid "Unlink without deleting messages."
#~ msgstr "Αποσύνδεση χωρίς διαγραφή μηνυμάτων"

#, fuzzy
#~ msgid "Messages and notifications for calls."
#~ msgstr "Αποστολή ειδοποιήσεων σχετικά με αντιδράσεις"

#, fuzzy
#~ msgid "Greatly refactored messages."
#~ msgstr "Καθαρισμός μηνυμάτων"

#, fuzzy
#~ msgid "Changed:"
#~ msgstr "Κανάλια"

#, fuzzy
#~ msgid "Notification support."
#~ msgstr "Γνωστοποιήσεις"

#, fuzzy
#~ msgid "Group storage."
#~ msgstr "Κρυπτογραφημένος αποθηκευτικός χώρος"

#, fuzzy
#~ msgid "Search for the channel view."
#~ msgstr "Αναζήτηση στα κανάλια"

#, fuzzy
#~ msgid "Message storage."
#~ msgstr "Προσθήκη μηνύματος"

#, fuzzy
#~ msgid "Prevented sending empty messages."
#~ msgstr "Πατήστε \"Enter\" για αποστολή μηνύματος"

#, fuzzy
#~ msgid "Keyboard shortcuts."
#~ msgstr "Εμφάνιση συντομεύσεων"

#, fuzzy
#~ msgid "Fix message receiving, sending attachments."
#~ msgstr "Αποστολή και λήψη συνημμένων"

#~ msgid "Load video"
#~ msgstr "Φόρτωση βίντεο"

#~ msgid "Load file"
#~ msgstr "Φόρτωση αρχείου"

#~ msgid "Load voice message"
#~ msgstr "Λήψη φωνητικού μηνύματος"

#~ msgid "Link Device"
#~ msgstr "Σύνδεση συσκευής"

#~ msgid "Please scan this QR code with your primary device."
#~ msgstr "Παρακαλώ σαρώστε αυτό το QR code με τη κυρίως συσκευή σας."

#~ msgid "Please wait until the contacts are displayed after linking."
#~ msgstr ""
#~ "Παρακαλώ περιμένετε μέχρι όλες οι επαφές να εμφανιστούν μετά την σύνδεση."

#~ msgid "An unofficial Signal GTK client"
#~ msgstr "Ένα ανεπίσημο πρόγραμμα Signal γραμμένο σε GTK"

#~ msgid ""
#~ "An unofficial GTK client for the messaging application Signal. This is a "
#~ "very simple application with many features missing compared to the "
#~ "official applications."
#~ msgstr ""
#~ "Ένα ανεπίσημο πρόγραμμα για την εφαρμογή μηνυμάτων Signal, γραμμένο σε "
#~ "GTK. Είναι μια απλή εφαρμογή με πολλές λειτουργίες που δεν υπάρχουν σε "
#~ "επίσημες εφαρμογές."

#~ msgid "Other"
#~ msgstr "Λοιπά"

#~ msgctxt "accessibility"
#~ msgid "Download"
#~ msgstr "Λήψη"

#~ msgid "React"
#~ msgstr "Αντίδραση"
