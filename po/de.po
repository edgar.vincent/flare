# English translations for Flare package.
# Copyright (C) 2022 THE Flare'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Flare package.
# Julian Schmidhuber <translation@schmiddi.anonaddy.com>, 2022.
# Jürgen Benvenuti <gastornis@posteo.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: 0.1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-09 13:04+0200\n"
"PO-Revision-Date: 2023-10-27 09:26+0000\n"
"Last-Translator: Markus Göllnitz <camelcasenick@bewares.it>\n"
"Language-Team: German <https://hosted.weblate.org/projects/"
"schmiddi-on-mobile/flare/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.2-dev\n"

#: data/resources/ui/about.blp.in:13
msgid "translator-credits"
msgstr ""
"Julian Schmidhuber <translation@schmiddi.anonaddy.com>\n"
"Jürgen Benvenuti <gastornis@posteo.org>, 2022\n"
"Leo Moser <leomoser99@gmail.com>, 2023"

#: data/resources/ui/channel_info_dialog.blp:5 data/resources/ui/window.blp:152
#: data/resources/ui/window.blp:148
msgid "Channel Information"
msgstr "Kanalinformationen"

#: data/resources/ui/channel_info_dialog.blp:28
msgctxt "accessibility"
msgid "Reset Session"
msgstr "Setze Verbindung zurück"

#: data/resources/ui/channel_info_dialog.blp:31
msgid "Reset Session"
msgstr "Setze Verbindung zurück"

#: data/resources/ui/channel_info_dialog.blp:40
#: data/resources/ui/error_dialog.blp:44
#: data/resources/ui/submit_captcha_dialog.blp:23
msgid "Close"
msgstr "Schließen"

#: data/resources/ui/channel_list.blp:11
#: data/resources/ui/channel_messages.blp:15
msgid "Offline"
msgstr "Offline"

#: data/resources/ui/channel_list.blp:19
msgid "Search"
msgstr "Suchen"

#: data/resources/ui/channel_list.blp:22 data/resources/ui/window.blp:60
#: data/resources/ui/window.blp:53
msgctxt "accessibility"
msgid "Search"
msgstr "Suchen"

#: data/resources/ui/channel_messages.blp:21
msgid "No Channel Selected"
msgstr "Kein Kanal ausgewählt"

#: data/resources/ui/channel_messages.blp:22
msgid "Select a channel"
msgstr "Einen Kanal auswählen"

#: data/resources/ui/channel_messages.blp:91
msgctxt "accessibility"
msgid "Remove the reply"
msgstr "Antwort entfernen"

#: data/resources/ui/channel_messages.blp:94
msgctxt "tooltip"
msgid "Remove reply"
msgstr "Antwort entfernen"

#: data/resources/ui/channel_messages.blp:119
#: data/resources/ui/channel_messages.blp:118
msgctxt "accessibility"
msgid "Remove an attachment"
msgstr "Anhang entfernen"

#: data/resources/ui/channel_messages.blp:122
#: data/resources/ui/channel_messages.blp:121
msgctxt "tooltip"
msgid "Remove attachment"
msgstr "Anhang entfernen"

#: data/resources/ui/channel_messages.blp:153
msgctxt "accessibility"
msgid "Add an attachment"
msgstr "Anhang hinzufügen"

#: data/resources/ui/channel_messages.blp:160
#: data/resources/ui/channel_messages.blp:156
msgctxt "tooltip"
msgid "Add attachment"
msgstr "Anhang hinzufügen"

#: data/resources/ui/channel_messages.blp:176
#: data/resources/ui/channel_messages.blp:182
msgctxt "accessibility"
msgid "Message input"
msgstr "Nachricht eingeben"

#: data/resources/ui/channel_messages.blp:182
#: data/resources/ui/channel_messages.blp:188
msgctxt "tooltip"
msgid "Message input"
msgstr "Nachricht eingeben"

#: data/resources/ui/channel_messages.blp:192
#: data/resources/ui/channel_messages.blp:194
msgctxt "accessibility"
msgid "Send"
msgstr "Senden"

#: data/resources/ui/channel_messages.blp:195
#: data/resources/ui/channel_messages.blp:197
msgctxt "tooltip"
msgid "Send"
msgstr "Senden"

#: data/resources/ui/dialog_clear_messages.blp:5
#: data/resources/ui/window.blp:123 data/resources/ui/window.blp:124
msgid "Clear messages"
msgstr "Nachrichten löschen"

#: data/resources/ui/dialog_clear_messages.blp:6
msgid ""
"This will clear all locally stored messages. This will also close the "
"application."
msgstr ""
"Dies wird alle auf dem Gerät vorhandenen Nachrichten löschen und die "
"Anwendung schließen."

#: data/resources/ui/dialog_clear_messages.blp:11
#: data/resources/ui/dialog_unlink.blp:11 src/gui/window.rs:288
msgid "Cancel"
msgstr "Abbrechen"

#: data/resources/ui/dialog_clear_messages.blp:12
msgid "Clear"
msgstr "Löschen"

#: data/resources/ui/dialog_unlink.blp:5 data/resources/ui/window.blp:118
#: data/resources/ui/window.blp:119
msgid "Unlink"
msgstr "Entkoppeln"

#: data/resources/ui/dialog_unlink.blp:6
msgid ""
"This will unlink this device and remove all locally saved data. This will "
"also close the application such that it can be relinked when opening it "
"again."
msgstr ""
"Dies wird dieses Gerät entkoppeln und alle lokal gespeicherten Daten "
"löschen. Dies wird außerdem die Anwendung schließen, sodass es nach dem "
"erneuten Öffnen neu gekoppelt werden kann."

#: data/resources/ui/dialog_unlink.blp:12
msgid "Unlink and delete all messages"
msgstr "Entkoppeln und alle Nachrichten löschen"

#: data/resources/ui/dialog_unlink.blp:13
msgid "Unlink but keep messages"
msgstr "Entkoppeln, aber Nachrichten behalten"

#: data/resources/ui/error_dialog.blp:5
msgid "Error"
msgstr "Fehler"

#: data/resources/ui/error_dialog.blp:26
msgid "Please consider reporting this error."
msgstr "Bitte erwäge, diesen Fehler zu melden."

#: data/resources/ui/error_dialog.blp:45
msgid "Report"
msgstr "Melden"

#: data/resources/ui/link_window.blp:20 data/resources/ui/link_window.blp:28
#: data/resources/ui/link_window.blp:81 data/resources/ui/link_window.blp:29
msgid "Link device"
msgstr "Gerät koppeln"

#: data/resources/ui/link_window.blp:33 data/resources/ui/link_window.blp:34
msgid "Scan Code"
msgstr "Code scannen"

#: data/resources/ui/link_window.blp:34 data/resources/ui/link_window.blp:35
msgid "Scan this code with another Signal app logged into your account."
msgstr ""
"Scannen Sie diesen Code mit einer anderen Signal-App, die bereits mit Ihrem "
"Konto angemeldet ist."

#: data/resources/ui/link_window.blp:58 data/resources/ui/link_window.blp:59
msgid "Link without scanning"
msgstr "Verbinden ohne zu scannen"

#: data/resources/ui/link_window.blp:64
msgid ""
"Note: Currently, to sucessfully link contacts requires intervention. Please "
"follow these steps: \n"
"- Link Flare as usual. Wait ~1min after the link window closed. \n"
"- Make sure you do not have running in the background enabled. Close and "
"restart Flare. \n"
"- In the primary menu, click 'synchronize contacts'. Wait ~1min. \n"
"- Make sure you do not have running in the background enabled. Close and "
"restart Flare. \n"
"- Your contacts should be behind the pencil-icon in the top-left."
msgstr ""
"Hinweis: Um Kontakte erfolgreich zu verknüpfen, ist derzeit ein Eingriff "
"erforderlich. Bitte folgen Sie diesen Schritten:\n"
"- Verknüpfen Sie Flare wie gewohnt. Warten Sie ~1min nachdem das Link-"
"Fenster geschlossen wurde.\n"
"- Vergewissern Sie sich, dass Sie die Option \"Im Hintergrund laufen\" nicht "
"aktiviert haben. Schließen Sie Flare und starten Sie es neu.\n"
"- Klicken Sie im Hauptmenü auf 'Kontakte synchronisieren'. Warten Sie ~1 "
"Minute.\n"
"- Vergewissern Sie sich, dass Sie die Option \"Im Hintergrund laufen\" nicht "
"aktiviert haben. Schließen Sie Flare und starten Sie es neu.\n"
"- Ihre Kontakte sollten nun hinter dem Stift-Symbol oben links zu sehen sein."

#: data/resources/ui/link_window.blp:74
#, fuzzy
msgid "Manual linking"
msgstr "Manuelles Verbinden"

#: data/resources/ui/link_window.blp:86
msgid "Manual Linking"
msgstr "Manuelles Verbinden"

#: data/resources/ui/link_window.blp:87
msgid ""
"If your device can't scan the QR code, you can manually enter the link "
"provided here"
msgstr ""
"Wenn Ihr Gerät den QR-Code nicht scannen kann, können Sie manuell den Link "
"hier eingeben"

#: data/resources/ui/link_window.blp:98
msgid "Copy to clipboard"
msgstr "In die Zwischenablage kopieren"

#: data/resources/ui/message_item.blp:12
msgid "Reply"
msgstr "Antworten"

#: data/resources/ui/message_item.blp:19
msgid "Delete"
msgstr "Löschen"

#: data/resources/ui/message_item.blp:27
msgid "Copy"
msgstr "Kopieren"

#: data/resources/ui/message_item.blp:36
msgid "Save as…"
msgstr "Speichern als…"

#: data/resources/ui/message_item.blp:44
msgid "Open with…"
msgstr "Öffnen mit…"

#: data/resources/ui/preferences_window.blp:6
msgid "General"
msgstr "Allgemein"

#: data/resources/ui/preferences_window.blp:10
msgid "Linking"
msgstr "Koppelung"

#: data/resources/ui/preferences_window.blp:11
msgid "The device will need to be relinked to take effect."
msgstr ""
"Das Gerät muss neu gekoppelt werden, damit diese Einstellung wirksam wird."

#: data/resources/ui/preferences_window.blp:14
msgid "Device Name"
msgstr "Name des Gerätes"

#. Translators: Description of Flare in metainfo: Features
#: data/resources/ui/preferences_window.blp:19
#: data/de.schmidhuberj.Flare.metainfo.xml:51
msgid "Attachments"
msgstr "Anhänge"

#: data/resources/ui/preferences_window.blp:22
msgid "Dowload images automatically"
msgstr "Bilder automatisch herunterladen"

#: data/resources/ui/preferences_window.blp:26
#: data/resources/ui/preferences_window.blp:35
msgid "Dowload videos automatically"
msgstr "Videos automatisch herunterladen"

#: data/resources/ui/preferences_window.blp:30
#: data/resources/ui/preferences_window.blp:48
msgid "Dowload files automatically"
msgstr "Dateien automatisch herunterladen"

#: data/resources/ui/preferences_window.blp:34
#: data/resources/ui/preferences_window.blp:61
msgid "Dowload voice messages automatically"
msgstr "Sprachnachrichten automatisch herunterladen"

#: data/resources/ui/preferences_window.blp:39
#: data/resources/ui/preferences_window.blp:75
msgid "Notifications"
msgstr "Benachrichtigungen"

#: data/resources/ui/preferences_window.blp:40
#: data/resources/ui/preferences_window.blp:76
msgid "Notifications for new messages."
msgstr "Benachrichtigungen für neue Nachrichten."

#: data/resources/ui/preferences_window.blp:43
#: data/de.schmidhuberj.Flare.gschema.xml:41
#: data/resources/ui/preferences_window.blp:79
msgid "Send notifications"
msgstr "Benachrichtigungen senden"

#: data/resources/ui/preferences_window.blp:47
#: data/resources/ui/preferences_window.blp:92
msgid "Send notifications on reactions"
msgstr "Benachrichtigungen zu Reaktionen senden"

#: data/resources/ui/preferences_window.blp:52 src/gui/preferences_window.rs:29
#: data/resources/ui/preferences_window.blp:106
msgid "Watch for new messages while closed"
msgstr "Empfange Benachrichtigungen im Hintergrund"

#: data/resources/ui/preferences_window.blp:60
#: data/resources/ui/preferences_window.blp:122
msgid "Mobile Compatibility"
msgstr "Kompatibilität mit mobilen Geräten"

#: data/resources/ui/preferences_window.blp:61
#: data/resources/ui/preferences_window.blp:123
msgid ""
"These options may want to be changed for a better experience on touch- and "
"mobile devices. The default values of those preferences are chosen to be "
"useful on desktop devices."
msgstr ""
"Diese Optionen können geändert werden, um eine bessere Nutzung auf Touch- "
"und Mobilgeräten zu ermöglichen. Die Standardwerte dieser Einstellungen sind "
"so gewählt, dass sie auf Desktop-Geräten nützlich sind."

#: data/resources/ui/preferences_window.blp:64
#: data/resources/ui/preferences_window.blp:126
msgid "Selectable Message Text"
msgstr "Auswählbarer Nachrichtentext"

#: data/resources/ui/preferences_window.blp:65
#: data/resources/ui/preferences_window.blp:127
msgid "Selectable text can interfere with swipe-gestures on touch-screens."
msgstr ""
"Auswählbarer Text kann mit Wischgesten auf Touchscreens Probleme bereiten."

#: data/resources/ui/preferences_window.blp:69
#: data/resources/ui/preferences_window.blp:140
msgid "Press \"Enter\" to send message"
msgstr "Drücke \"Enter\" um die Nachricht zu senden"

#: data/resources/ui/preferences_window.blp:141
msgid ""
"It may not be possible to send messages with newlines on touch keyboards not "
"allowing \"Control + Enter\""
msgstr ""
"Auf Touch-Tastaturen, die die Tastenkombination \"Strg + Enter\" nicht "
"zulassen, ist es möglicherweise nicht möglich, Nachrichten mit "
"Zeilenumbrüchen zu versenden"

#: data/resources/ui/shortcuts.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Allgemein"

#: data/resources/ui/shortcuts.blp:14
msgctxt "shortcut window"
msgid "Show shortcuts"
msgstr "Tastaturkürzel anzeigen"

#: data/resources/ui/shortcuts.blp:19
msgctxt "shortcut window"
msgid "Go to settings"
msgstr "Zu den Einstellungen springen"

#: data/resources/ui/shortcuts.blp:24
msgctxt "shortcut window"
msgid "Go to about page"
msgstr "Zu Info springen"

#: data/resources/ui/shortcuts.blp:29
msgctxt "shortcut window"
msgid "Open menu"
msgstr "Menü öffnen"

#: data/resources/ui/shortcuts.blp:34
msgctxt "shortcut window"
msgid "Close window"
msgstr "Fenster Schließen"

#: data/resources/ui/shortcuts.blp:40
msgctxt "shortcut window"
msgid "Channels"
msgstr "Kanäle"

#: data/resources/ui/shortcuts.blp:43
msgctxt "shortcut window"
msgid "Go to channel 1…9"
msgstr "Zu Kanal 1…9 springen"

#: data/resources/ui/shortcuts.blp:48
msgctxt "shortcut window"
msgid "Search in channels"
msgstr "In den Kanälen suchen"

#: data/resources/ui/shortcuts.blp:53
msgctxt "shortcut window"
msgid "Upload attachment"
msgstr "Anhang hochladen"

#: data/resources/ui/shortcuts.blp:58
msgctxt "shortcut window"
msgid "Focus the text input box"
msgstr "Die Texteingabe fokussieren"

#: data/resources/ui/shortcuts.blp:63
msgctxt "shortcut window"
msgid "Load more messages"
msgstr "Weitere Nachrichten laden"

#: data/resources/ui/text_entry.blp:12
msgid "Message"
msgstr "Nachricht eingeben"

#: data/resources/ui/text_entry.blp:38
#: data/resources/ui/channel_messages.blp:172
msgctxt "accessibility"
msgid "Insert an emoji"
msgstr "Füge ein Emoji ein"

#: data/resources/ui/text_entry.blp:44
#: data/resources/ui/channel_messages.blp:175
msgctxt "tooltip"
msgid "Insert emoji"
msgstr "Füge ein Emoji ein"

#: data/resources/ui/window.blp:25
#, fuzzy
msgid "Channel list"
msgstr "Kanäle"

#: data/resources/ui/window.blp:37 data/resources/ui/window.blp:30
msgctxt "accessibility"
msgid "Add Conversation"
msgstr "Unterhaltung Hinzufügen"

#: data/resources/ui/window.blp:48 data/resources/ui/window.blp:91
#: data/resources/ui/window.blp:41 data/resources/ui/window.blp:94
msgctxt "accessibility"
msgid "Menu"
msgstr "Menü"

#: data/resources/ui/window.blp:78
msgid "Chat"
msgstr "Chat"

#: data/resources/ui/window.blp:113 data/resources/ui/window.blp:114
msgid "Settings"
msgstr "Einstellungen"

#: data/resources/ui/window.blp:128
#: data/resources/ui/submit_captcha_dialog.blp:5
#: data/resources/ui/window.blp:129
msgid "Submit Captcha"
msgstr "Captcha absenden"

#: data/resources/ui/window.blp:133
msgid "Synchronize Contacts"
msgstr "Kontakte synchronisieren"

#: data/resources/ui/window.blp:138 data/resources/ui/window.blp:134
msgid "Keybindings"
msgstr "Tastaturkürzel"

#: data/resources/ui/window.blp:143 data/resources/ui/window.blp:139
msgid "About"
msgstr "Info"

#: data/resources/ui/window.blp:156
#, fuzzy
msgid "Clear Messages"
msgstr "Nachrichten löschen"

#. Translators: Do not translate tags around "on this website".
#: data/resources/ui/submit_captcha_dialog.blp:7
msgid ""
"Submit a Captcha challenge. The token can be obtained from the error message "
"that was displayed from Flare. The captcha must be filled out <a "
"href=\"https://signalcaptchas.org/challenge/generate.html\">on this website</"
"a> and the link to open Signal must be pasted to the corresponding entry. "
"Note that the captcha is only valid for about one minute."
msgstr ""
"Submit a Captcha challenge. The token can be obtained from the error message "
"that was displayed from Flare. Das Captcha muss <a href=\"https://"
"signalcaptchas.org/challenge/generate.html\">auf dieser Website</a> "
"ausgefüllt werden und der Link zum Öffnen von Signal muss in den "
"entsprechenden Eintrag eingefügt werden. Note that the captcha is only valid "
"for about one minute."

#: data/resources/ui/submit_captcha_dialog.blp:13
msgid "Token"
msgstr "Token"

#: data/resources/ui/submit_captcha_dialog.blp:16
msgid "Captcha"
msgstr "Captcha"

#: data/resources/ui/submit_captcha_dialog.blp:24
msgid "Submit"
msgstr "Absenden"

#: src/backend/channel.rs:581 src/backend/channel.rs:564
msgid "Note to self"
msgstr "Notiz an mich"

#: src/backend/manager.rs:430 src/backend/manager.rs:411
msgid "You"
msgstr "Du"

#: src/backend/message/call_message.rs:115
msgid "Started calling."
msgstr "Anruf wurde begonnen."

#: src/backend/message/call_message.rs:116
msgid "Answered a call."
msgstr "Ein Anruf wurde beantwortet."

#: src/backend/message/call_message.rs:117
msgid "Hung up."
msgstr "Aufgelegt."

#: src/backend/message/call_message.rs:118
msgid "Is busy."
msgstr "Besetzt."

#. Translators: When receiving a reaction message in a group, this will be the text to format the notification. Do not translate the text in {}.
#: src/backend/message/reaction_message.rs:70
msgid "{sender} reacted {emoji} to a message."
msgstr "{sender} hat {emoji} auf eine Nachricht reagiert."

#. Translators: When receiving a reaction message in a 1-to-1 channel (the body, the sender will be in the notification title and is not included in the translation), this will be the text to format the notification. Do not translate the text in {}.
#: src/backend/message/reaction_message.rs:76
msgid "Reacted {emoji} to a message."
msgstr "Reagierte {emoji} auf eine Nachricht."

#: src/backend/message/text_message.rs:369
#: src/backend/message/text_message.rs:365
msgid "Sent an image"
msgid_plural "Sent {} images"
msgstr[0] "Hat ein Bild gesendet"
msgstr[1] "Hat {} Bilder gesendet"

#: src/backend/message/text_message.rs:372
#: src/backend/message/text_message.rs:368
msgid "Sent an video"
msgid_plural "Sent {} videos"
msgstr[0] "Hat ein Video gesendet"
msgstr[1] "Hat {} Videos gesendet"

#: src/backend/message/text_message.rs:376
#: src/backend/message/text_message.rs:372
msgid "Sent a voice message"
msgid_plural "Sent {} voice messages"
msgstr[0] "Hat eine Sprachnachricht gesendet"
msgstr[1] "Hat {} Sprachnachrichten gesendet"

#: src/backend/message/text_message.rs:382
#: src/backend/message/text_message.rs:378
msgid "Sent a file"
msgid_plural "Sent {} files"
msgstr[0] "Hat eine Datei gesendet"
msgstr[1] "Hat {} Dateien gesendet"

#: src/error.rs:87
msgid "I/O Error."
msgstr "Ein-/Ausgabefehler."

#: src/error.rs:92
msgid "There does not seem to be a connection to the internet available."
msgstr "Es scheint keine Internetverbindung vorhanden zu sein."

#: src/error.rs:97
msgid "Something glib-related failed."
msgstr "Etwas glib-bezogenes ist fehlgeschlagen."

#: src/error.rs:102
msgid "The communication with Libsecret failed."
msgstr "Die Kommunikation mit Libsecret ist fehlgeschlagen."

#: src/error.rs:107
msgid ""
"The backend database failed. Please restart the application or delete the "
"database and relink the application."
msgstr ""
"In der Datenbank ist ein Fehler aufgetreten. Bitte starte die Anwendung neu "
"oder lösche die Datenbank und kopple die Anwendung erneut."

#: src/error.rs:112
msgid ""
"You do not seem to be authorized with Signal. Please delete the database and "
"relink the application."
msgstr ""
"Du scheinst nicht von Signal autorisiert zu sein. Bitte lösche die Datenbank "
"und kopple die Anwendung erneut."

#: src/error.rs:117
msgid "Sending a message failed."
msgstr "Nachricht konnte nicht gesendet werden."

#: src/error.rs:122
msgid "Receiving a message failed."
msgstr "Nachricht konnte nicht empfangen werden."

#: src/error.rs:127
msgid ""
"Something unexpected happened with the signal backend. Please retry later."
msgstr ""
"Etwas Unerwartetes ist mit Signal passiert. Bitte versuche es später wieder."

#: src/error.rs:132
msgid "The application seems to be misconfigured."
msgstr "Die Anwendung scheint falsch konfiguriert zu sein."

#: src/error.rs:137
msgid "A part of the application crashed."
msgstr "Ein Teil der Anwendung ist abgestürzt."

#: src/error.rs:147
msgid "Please check your internet connection."
msgstr "Bitte prüfe deine Internetverbindung."

#: src/error.rs:152
msgid "Please delete the database and relink the device."
msgstr "Bitte lösche die Datenbank und kopple das Gerät erneut."

#: src/error.rs:159
msgid "The database path at {} is no folder."
msgstr "Der Datenbankpfad bei {} ist kein Ordner."

#: src/error.rs:164
msgid "Please restart the application with logging and report this issue."
msgstr ""
"Bitte starte die Anwendung neu mit Protokollierung und melde dieses Problem."

#: src/gui/link_window.rs:67 src/gui/link_window.rs:65
msgid "Copied to clipboard"
msgstr "In die Zwischenablage kopiert"

#: src/gui/preferences_window.rs:52 src/gui/preferences_window.rs:51
msgid "Background permission"
msgstr "Hintergrundberechtigung"

#: src/gui/preferences_window.rs:53 src/gui/preferences_window.rs:52
msgid "Use settings to remove permissions"
msgstr "Nutze die Einstellungen um diese Berechtigung zu entfernen"

#. How to format time. Should probably be %H:%M (meaning print hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:88 src/gui/utility.rs:112 src/gui/utility.rs:58
msgid "%H:%M"
msgstr "%H:%M"

#. How to format a date with time. Should probably be similar to %Y-%m-%d %H:%M (meaning print year, month from 01-12, day from 01-31 (each separated by -), hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:91 src/gui/utility.rs:61
msgid "%Y-%m-%d %H:%M"
msgstr "%d.%m.%Y %H:%M"

#: src/gui/utility.rs:124
msgid "Today"
msgstr "Heute"

#: src/gui/utility.rs:126
msgid "Yesterday"
msgstr "Gestern"

#. How to format a human-readable date including the year. Should probably be similar to %Y-%m-%d (meaning print year, month from 01-12, day from 01-31 (each separated by -)).
#. For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:132
msgid "%Y-%m-%d"
msgstr "%d.%m.%Y"

#. How to format a human-readable date excluding the year. Should probably be similar to "%a., %d. %b" (meaning print abbreviated weekday, day from 1-31 and abbreviated month name).
#. For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:136
msgid "%a., %e. %b"
msgstr "%a., %e. %b"

#: src/gui/window.rs:283 src/gui/window.rs:289
#, fuzzy
msgid "Remove Messages"
msgstr "Nachrichten empfangen"

#: src/gui/window.rs:284
#, fuzzy
msgid "This will remove all locally stored messages from this channel"
msgstr ""
"Dies wird alle auf dem Gerät vorhandenen Nachrichten löschen und die "
"Anwendung schließen."

#. Translators: Flare name in metainfo.
#: data/de.schmidhuberj.Flare.desktop.in.in:5
#: data/de.schmidhuberj.Flare.metainfo.xml:8
msgid "Flare"
msgstr "Flare"

#. Translators: Flare summary in metainfo.
#: data/de.schmidhuberj.Flare.desktop.in.in:6
#: data/de.schmidhuberj.Flare.metainfo.xml:10
msgid "Chat with your friends on Signal"
msgstr "Chatte mit deinen Freunden auf Signal"

#. Translators: Description of Flare in metainfo.
#: data/de.schmidhuberj.Flare.metainfo.xml:26
msgid ""
"Flare is an unofficial app that lets you chat with your friends on Signal "
"from Linux. It is still in development and doesn't include as many features "
"as the official Signal applications."
msgstr ""
"Flare ist eine inoffizielle Anwendung, mit der Sie von Linux aus mit Ihren "
"Freunden über Signal chatten können. Sie befindet sich noch in der "
"Entwicklung und bietet nicht so viele Funktionen wie die offiziellen Signal-"
"Anwendungen."

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:31
msgid "Linking device"
msgstr "Gerät koppeln"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:35
msgid "Sending messages"
msgstr "Nachrichten senden"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:39
msgid "Receiving messages"
msgstr "Nachrichten empfangen"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:43
msgid "Replying to a message"
msgstr "Auf eine Nachricht antworten"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:47
msgid "Reacting to a message"
msgstr "Auf eine Nachricht reagieren"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:55
msgid "Encrypted storage"
msgstr "Verschlüsselte Datenspeicherung"

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:59
msgid "Notifications, optionally in the background"
msgstr "Benachrichtigungen, optional im Hintergrund"

#. Translators: Description of Flare in metainfo: Security note
#: data/de.schmidhuberj.Flare.metainfo.xml:64
msgid ""
"More features are planned, blocked, or not-planned. Consult the README for "
"more information about them."
msgstr ""
"Weitere Funktionen sind geplant, blockiert oder nicht geplant. In der README "
"finden Sie weitere Informationen dazu."

#. Translators: Description of Flare in metainfo: Security note
#: data/de.schmidhuberj.Flare.metainfo.xml:68
msgid ""
"Please note that using this application will probably worsen your security "
"compared to using official Signal applications. Use with care when handling "
"sensitive data. Look at the projects README for more information about "
"security."
msgstr ""
"Bitte beachten Sie, dass die Verwendung dieser Anwendung Ihre Sicherheit im "
"Vergleich zur Verwendung offizieller Signal-Anwendungen wahrscheinlich "
"verschlechtert. Seien Sie vorsichtig im Umgang mit sensiblen Daten. In der "
"README des Projekts finden Sie weitere Informationen zur Sicherheit."

#: data/de.schmidhuberj.Flare.metainfo.xml:499
#: data/de.schmidhuberj.Flare.metainfo.xml:479
msgid "Overview of the application"
msgstr "Übersicht der Anwendung"

#: data/de.schmidhuberj.Flare.gschema.xml:6
msgid "Window width"
msgstr "Fensterbreite"

#: data/de.schmidhuberj.Flare.gschema.xml:10
msgid "Window height"
msgstr "Fensterhöhe"

#: data/de.schmidhuberj.Flare.gschema.xml:14
msgid "Window maximized state"
msgstr "Maximierter Zustand des Fensters"

#: data/de.schmidhuberj.Flare.gschema.xml:19
msgid ""
"The device name when linking Flare. This requires the application to be re-"
"linked."
msgstr ""
"Der Gerätename beim Verknüpfen von Flare. Dies erfordert, dass die Anwendung "
"neu gekoppelt wird."

#: data/de.schmidhuberj.Flare.gschema.xml:24
msgid "Automatically download images"
msgstr "Bilder automatisch herunterladen"

#: data/de.schmidhuberj.Flare.gschema.xml:28
msgid "Automatically download videos"
msgstr "Videos automatisch herunterladen"

#: data/de.schmidhuberj.Flare.gschema.xml:32
msgid "Automatically download voice messages"
msgstr "Sprachnachrichten automatisch herunterladen"

#: data/de.schmidhuberj.Flare.gschema.xml:36
msgid "Automatically download files"
msgstr "Dateien automatisch herunterladen"

#: data/de.schmidhuberj.Flare.gschema.xml:45
msgid "Run in background when closed"
msgstr "Nach dem Schließen im Hintergrund ausführen"

#: data/de.schmidhuberj.Flare.gschema.xml:49
msgid "Send notifications when retrieving reactions"
msgstr "Benachrichtigungen senden beim Abrufen von Reaktionen"

#: data/de.schmidhuberj.Flare.gschema.xml:54
msgid "Can messages be selected"
msgstr "Nachrichten können ausgewählt werden"

#: data/de.schmidhuberj.Flare.gschema.xml:58
msgid "Send a message when the Enter-key is pressed"
msgstr "Eine Nachricht senden, wenn die Eingabetaste gedrückt wird"

#: data/resources/ui/channel_messages.blp:198
msgid "Send"
msgstr "Senden"

#: data/resources/ui/message_item.blp:156
msgid "Load media"
msgstr "Medien laden"

#~ msgid "Signal GTK Client"
#~ msgstr "Signal GTK-Client"

#, fuzzy
#~ msgid "Changed"
#~ msgstr "Kanäle"

#, fuzzy
#~ msgid "Notifications on reactions received (configurable)."
#~ msgstr "Benachrichtigungen zu Reaktionen senden"

#, fuzzy
#~ msgid "Button to scroll down in the messages view."
#~ msgstr "Sprachnachrichten automatisch herunterladen"

#, fuzzy
#~ msgid "Keyboard shortcut Ctrl+Q to close window."
#~ msgstr "Tastaturkürzel anzeigen"

#, fuzzy
#~ msgid "Message deletion."
#~ msgstr "Nachrichten-Speicher"

#, fuzzy
#~ msgid "Updated application icon and emblem."
#~ msgstr "Ein Teil der Anwendung ist abgestürzt."

#, fuzzy
#~ msgid "Copy action in message menu."
#~ msgstr "Auf eine Nachricht antworten"

#, fuzzy
#~ msgid "Unlink without deleting messages."
#~ msgstr "Entkoppeln, aber Nachrichten behalten"

#, fuzzy
#~ msgid "Messages and notifications for calls."
#~ msgstr "Benachrichtigungen senden"

#, fuzzy
#~ msgid "Greatly refactored messages."
#~ msgstr "Nachrichten löschen"

#, fuzzy
#~ msgid "Changed:"
#~ msgstr "Kanäle"

#, fuzzy
#~ msgid "UI improvements for the channel list."
#~ msgstr "In den Kanälen suchen"

#, fuzzy
#~ msgid "Notification support."
#~ msgstr "Benachrichtigungen"

#, fuzzy
#~ msgid "New message storage backend."
#~ msgstr "Nachrichten-Speicher"

#, fuzzy
#~ msgid "Group storage."
#~ msgstr "Verschlüsselte Datenspeicherung"

#, fuzzy
#~ msgid "Search for the channel view."
#~ msgstr "In den Kanälen suchen"

#, fuzzy
#~ msgid "Message storage."
#~ msgstr "Nachrichten-Speicher"

#, fuzzy
#~ msgid "Keyboard shortcuts."
#~ msgstr "Tastaturkürzel anzeigen"

#, fuzzy
#~ msgid "Fix message receiving, sending attachments."
#~ msgstr "Senden und Empfangen von Anhängen"

#~ msgid "Load image"
#~ msgstr "Bild laden"

#~ msgid "Load video"
#~ msgstr "Video laden"

#~ msgid "Load file"
#~ msgstr "Datei laden"

#~ msgid "Load voice message"
#~ msgstr "Sprachnachricht laden"

#~ msgid "Link Device"
#~ msgstr "Gerät koppeln"

#~ msgid "Please scan this QR code with your primary device."
#~ msgstr "Bitte scanne diesen QR-Code mit deinem Hauptgerät."

#~ msgid "Please wait until the contacts are displayed after linking."
#~ msgstr "Bitte warte nach dem Koppeln, bis deine Kontakte angezeigt werden."

#~ msgid "An unofficial Signal GTK client"
#~ msgstr "Ein inoffizieller Signal GTK-Client"

#~ msgid ""
#~ "An unofficial GTK client for the messaging application Signal. This is a "
#~ "very simple application with many features missing compared to the "
#~ "official applications."
#~ msgstr ""
#~ "Ein inoffizieller GTK-Client für die Messaging-Anwendung Signal. Es "
#~ "handelt sich um eine sehr einfache Anwendung mit vielen fehlenden "
#~ "Funktionen im Vergleich zu den offiziellen Anwendungen."

#~ msgid "Other"
#~ msgstr "Andere"

#~ msgctxt "accessibility"
#~ msgid "Download"
#~ msgstr "Herunterladen"

#~ msgid "React"
#~ msgstr "Reagieren"

#~ msgctxt "accessibility"
#~ msgid "Reset Identity"
#~ msgstr "Identität zurücksetzen"

#~ msgid "Reset Identity"
#~ msgstr "Identität zurücksetzen"

#~ msgid "Message Storage"
#~ msgstr "Nachrichten-Speicher"

#~ msgid "Number of messages to load on startup"
#~ msgstr "Anzahl der Nachrichten, welche bei Anwendungsstart geladen werden"

#~ msgid "Per channel"
#~ msgstr "Pro Kanal"

#~ msgid "Number of messages to load on request"
#~ msgstr "Anzahl der Nachrichten, welche auf Anfrage geladen werden"

#~ msgid "The amount of messages to load on startup for each channel"
#~ msgstr ""
#~ "Anzahl der Nachrichten, welche bei Anwendungsstart für jeden Kanal "
#~ "geladen werden"

#~ msgid "The amount of messages to load on request for each channel"
#~ msgstr ""
#~ "Anzahl der Nachrichten, welche auf Anfrage für jeden Kanal geladen werden"

#~ msgid ""
#~ "Notifications for new messages. These notifications will only work while "
#~ "Flare is open, if you also want to be notified when this application is "
#~ "closed, consider using something like messenger-notify."
#~ msgstr ""
#~ "Benachrichtigungen für neue Nachrichten. Diese Benachrichtigungen "
#~ "funktionieren nur, wenn Flare geöffnet ist. Wenn du auch benachrichtigt "
#~ "werden willst, wenn das Programm geschlossen ist, nutze beispielsweise "
#~ "messenger-notify."

#~ msgid "Are you sure?"
#~ msgstr "Bist du sicher?"

#~ msgid "For each channel"
#~ msgstr "Für jeden Kanal"
